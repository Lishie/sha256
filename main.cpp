#include <iostream>
#include <cstdint>
#include <cstring>

typedef unsigned int word;

inline word sha_ch(word &x, word &y, word &z) {
	return (x & y) ^ ((~x) & z);
}

inline word sha_maj(word &x, word &y, word &z) {
	return (x & y) ^ (x & z) ^ (y & z);
}

inline word sha_e0(word &x) {
	return ((x >> 2 ) | (x << 30)) ^ ((x >> 13 ) | (x << 19)) ^ ((x >> 22 ) | (x << 10));
}

inline word sha_e1(word &x) {
	return ((x >> 6 ) | (x << 26)) ^ ((x >> 11 ) | (x << 21)) ^ ((x >> 25 ) | (x << 7));
}

inline word sha_o0(word &x) {
	return ((x >> 7 ) | (x << 25)) ^ ((x >> 18 ) | (x << 14)) ^ (x >> 3);
}

inline word sha_o1(word &x) {
	return ((x >> 17 ) | (x << 15)) ^ ((x >> 19 ) | (x << 13)) ^ (x >> 10);
}

//printing an array of words, for testing purposes
void printArr(word* arr, int len) {
	std::cout << std::hex;
	for (int i = 0; len > i; ++i) {
		std::cout << arr[i] << " ";
		if( (i+1) % 8 == 0) {
			std::cout << std::endl;
		}
	}
	std::cout << std::dec;
}

int main(int argc, char const *argv[]) {
	const char* input;
	if(argc != 2) {
		std::cout << "Please secify the hashed data\n";
		return -1;
	}
	else {
		input = argv[1];
	}

	int len = strlen(input); // input length in bytes
	int blockLen = (int((len+8)/64+1));

	// initial hash value H(0)
	word intermHash[8] = {
		0x6a09e667,
		0xbb67ae85,
		0x3c6ef372,
		0xa54ff53a,
		0x510e527f,
		0x9b05688c,
		0x1f83d9ab,
		0x5be0cd19
	}; 

	word hash[8];
	for(int i = 0; 8 > i; ++i) {
		hash[i] = intermHash[i];
	}

	// constant words K(0..63)
	const word kconst[] = {
		0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
    0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
    0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
    0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a,
    0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
    0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
	};

	//parsing data into message blocks
	word* block = new word[blockLen*16];
	for(int i = 0; i < len/4+1; i++) {
		//i - current word
		block[i] = 0;
		for(int j = 0; j < 4; j++) {
			//j - byte of current word
			block[i] = block[i] << 8;
			if(input[i*4+j] == '\0') {
				block[i] = block[i] | 128;
				for(int k = 0; k < 3-j; k++) {
					block[i] = block[i] << 8;
				}
				break;
			}
			else {
				block[i] = block[i] | input[i*4+j];
			}
		}
	}
	block[blockLen*16-1] = len*8;


	// Expanded message blocks - W(0..63)
	word embs[64];
	
	/*
	std::cout << "input: ";
	printArr(block, blockLen*16);
	std::cout << "init: ";
	printArr(hash, 8);
	*/

	//  a   b   c   d   e   f   g   h
	//  0   1   2   3   4   5   6   7
	// processing blocks
	for(int i = 0; i < blockLen; i++) {
		// calc expanded message blocks
		for (int j = 0; 16 > j; ++j) {
			embs[j] = block[i*16+j];
		}
		for(int j = 16; 64 > j; ++j) {
			embs[j] = sha_o1(embs[j-2]) + embs[j-7] + sha_o0(embs[j-15]) + embs[j-16];
		}

		//hashing loop
		for (int j = 0; 64 > j; ++j) {
			word t1 = hash[7] + sha_e1(hash[4]) + sha_ch(hash[4], hash[5], hash[6]) + kconst[j] + embs[j];
			word t2 = sha_e0(hash[0]) + sha_maj(hash[0], hash[1], hash[2]);
			hash[7] = hash[6];
			hash[6] = hash[5];
			hash[5] = hash[4];
			hash[4] = hash[3] + t1;
			hash[3] = hash[2];
			hash[2] = hash[1];
			hash[1] = hash[0];
			hash[0] = t1 + t2;
		}
		for(int j = 0; j < 8; j++) {
			hash[j] = intermHash[j] + hash[j];
			intermHash[j] = hash[j];
		}
	}

	std::cout << std::hex;
	for(int i = 0; 8 > i; i++) {
		std::cout << hash[i];
	}
	std::cout << std::endl;

	return 0;
}
